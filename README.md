# Welcome to your CDK TypeScript project
This project provides a template for developing an AWS S3 bucket using the AWS Cloud Development Kit (CDK) with TypeScript. 

## Live Demo
The live demo of the deployed S3 bucket static website can be found [here](http://miniproject3stack-secondbucketac350874-qdyg8b2bb3lk.s3-website-us-east-1.amazonaws.com/).

## Create S3 bucket using AWS CDK
### Prerequisites
Before you begin, make sure you have the following installed:

- Node.js (>= 10.3.0)
- npm (comes with Node.js)
- AWS CLI
- AWS CDK Toolkit

If you haven't installed the AWS CLI and AWS CDK Toolkit, you can do so by running:

```bash
npm install -g aws-cli
npm install -g aws-cdk
```
### Create the app
#### Initialize the App
Now initialize the app by using the cdk init command. Specify the desired template ("app") and programming language as shown in the following examples:

```bash
cdk init app --language typescript
```
#### Edit Typescript Code

Then, add the S3 bucket to the stack. Open the lib/XXXX-stack.ts file and add the following code to the Stack class:

```typescript
import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';

export class MiniProject3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Add bucket properties like versioning and encryption
    const bucket = new cdk.aws_s3.Bucket(this, 'SecondBucket', {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED,

    });
  }
}
```

### Build the app:
To build the app, run the following command:
```bash
npm run build
```
### Other Useful commands for the app:
* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `npx cdk deploy`  deploy this stack to your default AWS account/region
* `npx cdk diff`    compare deployed stack with current state
* `npx cdk synth`   emits the synthesized CloudFormation template


#### CodeWhisper Usage
CodeWhisperer played a crucial role in the development of this CDK app. 

1. Code Suggestions: As I typed, CodeWhisperer provided relevant code suggestions, speeding up the development process by suggesting complete blocks of code for creating the S3 bucket.
2. Best Practices: It offered insights into AWS best practices, ensuring that the S3 bucket creation adhered to security and efficiency standards.
3. Error Reduction: By suggesting syntactically correct code, CodeWhisperer helped minimize errors, allowing me to focus on the logic and architecture of the application.
4. Here is a screenshot of the CodeWhisperer in action, with the code suggestions highlighted which could help me selecting proper removal policy of s3 Bucket.
![alt text](images/codewhisper.jpg)
    

### Generated S3 Bucket Screenshot:

![alt text](images/myfirstbucket_screenshot.png)

#### ScreenShot of S3 Bucket Properties
With the code above, a new S3 bucket is created with versioning enabled and server-side encryption using S3-managed keys.
![alt text](images/bucket_versioning.png)
![alt text](images/encryption_property.png)



## (Optional) Configuring a static website on Amazon S3
To host a static website on Amazon S3, you need to configure your bucket for website hosting. You can do this by adding a bucket policy that allows public read access to the objects in the bucket.

### Steps to configure a static website on Amazon S3
1. Open the Amazon S3 console at https://console.aws.amazon.com/s3/.
2. Choose the bucket that you want to use to host a static website.
3. Choose the Properties tab.
4. Choose Static website hosting.
5. Choose Use this bucket to host a website.
6. In the Index document box, type the name of your index document. The name is typically index.html.
8. Add a bucket policy to allow public read access to the objects in your bucket. 
   - Open the Permissions tab.
   - Choose Bucket Policy.
   - Add the following bucket policy to allow public read access to your bucket. Replace <bucket-name> with the name of your bucket.

    ```json
    {
    "Version": "2012-10-17",
    "Statement": [
        {
        "Sid": "PublicReadGetObject",
        "Effect": "Allow",
        "Principal": "*",
        "Action": "s3:GetObject",
        "Resource": "arn:aws:s3:::<bucket-name>/*"
        }
    ]
    }
    ```
9. Upload the index document to your bucket by dragging and dropping the index file into the console bucket listing. Choose Upload, and follow the prompts to choose and upload the index file.
10. Open the endpoint URL in a web browser to view your website. The endpoint URL is listed in the Static website hosting pane. It should look like this: http://<bucket-name>.s3-website-<region>.amazonaws.com.


